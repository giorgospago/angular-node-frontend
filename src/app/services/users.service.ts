import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  getUsers(key?: string) {
    return this.http.get<any[]>('http://localhost:3000/users/' + (key || ''));
  }

  deleteUser(id) {
    return this.http
      .request(
        'delete',
        'http://localhost:3000/users/delete',
        {body: {user_id: id}}
      );
  }

}
