import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {debounceTime} from 'rxjs/operators';

export interface IUser {
  _id: string|number;
  firstName: string;
  lastName: string;
  gender: string;
}

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  private url: string = "http://localhost:3000/search";

  constructor(
    private http: HttpClient
  ) { }

  searchUsers(key: string, db: string): Observable<IUser[]> {

    return this.http
      .get<IUser[]>(this.url + "/" + db + "/" + key)
      .pipe(debounceTime(500));

    // switch (db) {
    //   case "mysql":
    //     return this.http.get(this.url + "/mysql/" + key);
    //   case "mongo":
    //     return this.http.get(this.url + "/mongo/" + key);
    //   case "elastic":
    //     return this.http.get(this.url + "/elastic/" + key);
    // }
  }
}
