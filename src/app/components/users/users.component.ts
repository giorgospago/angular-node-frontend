import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  users: any[] = [];
  spinner: boolean;
  search: string = "";

  constructor(private us: UsersService) { }

  ngOnInit() {
    this.users = [];
    this.spinner = true;

    this.us.getUsers().subscribe(data => {
      this.users = data;
      this.spinner = false;
    });
  }


  removeUser(id) {
    this.spinner = true;
    this.us.deleteUser(id).subscribe(() => {

      this.us.getUsers().subscribe(data => {
        this.users = data;
        this.spinner = false;
      });

    });
  }

  searchUsers() {
    this.us.getUsers(this.search).subscribe(data => {
      this.users = data;
    });
  }

}
