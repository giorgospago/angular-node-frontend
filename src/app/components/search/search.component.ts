import { Component, OnInit } from '@angular/core';
import { SearchService, IUser } from '../../services/search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  public mysqlTime: number;
  public mysqlKey: string = "";
  public mysqlUsers: IUser[] = [];

  public mongoTime: number;
  public mongoKey: string = "";
  public mongoUsers: IUser[] = [];

  public elasticTime: number;
  public elasticKey: string = "";
  public elasticUsers: IUser[] = [];

  constructor(private ss: SearchService) { }

  ngOnInit() {
  }

  searchMysql() {
    const start = Date.now();
    this.ss.searchUsers(this.mysqlKey, "mysql")
      .subscribe(users => {
        this.mysqlUsers = users;
        this.mysqlTime = Date.now() - start;
      });
  }

  searchMongo() {
    const start = Date.now();
    this.ss.searchUsers(this.mongoKey, "mongo")
      .subscribe(users => {
        this.mongoUsers = users;
        this.mongoTime = Date.now() - start;
      });
  }

  searchElastic() {
    const start = Date.now();
    this.ss.searchUsers(this.elasticKey, "elastic")
      .subscribe(users => {
        this.elasticUsers = users;
        this.elasticTime = Date.now() - start;
      });
  }

}
